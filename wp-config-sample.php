<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '');

/** MySQL database username */
define('DB_USER', '');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8pLJx2Wqjtw99YDnn15imtbteU9T4j2OTf6EDjOg2oB1E1diM2c8ADtOLVBCcwdv');
define('SECURE_AUTH_KEY',  'RhSdRoWLSNuYNp9XKVq6PibQp1bPt3f475Q1NJHyx3T2j2QGK0QbihTrwly25x3u');
define('LOGGED_IN_KEY',    'fo5lmMxOTGQ8O4X9TvKDQREtYe5wXXyidV7WYzUAowbRsnbWCUsbMk9JaOfUNR4k');
define('NONCE_KEY',        'Cu3zPxQjIB8mLw9gyLxRFK49ueNK2P8pA8tVfxP5PTOPl12v2EsSYgJRAad9fpEA');
define('AUTH_SALT',        'Gjem0cixCXPFKuB8scLDNzpsOwV7FRuIzQL3eIxllWZbiUFEKs5uA8hwbxfyPF3c');
define('SECURE_AUTH_SALT', 'bX374C9DDUywSKRsUSo68AL6u3Ac5j3P5IB97KHp7KCAJQ2Dax5feMRaSN578ihx');
define('LOGGED_IN_SALT',   'WuroTKxOMee4CFleO8seQsAGQU4t9k9i3AXzw9vd7jL4Y379em6UbJEV1Gj6GDop');
define('NONCE_SALT',       'b5vnCF5Xb417Er4juuhC0317ixdfgB8z3OB8xao1KEoTRnl7479auvJdGbZob54S');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
