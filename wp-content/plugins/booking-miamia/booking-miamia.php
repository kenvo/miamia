<?php
/**
 * Plugin Name: Booking MiaMia
 * Plugin URI: http://www.newman.miamianewman.com.au/ 
 * Description: Plugin Booking Hotel
 * Version: 1.0 
 * Author: Garung
 * License: GPLv2
 */
define('NFBOOKING_URL', 'bfbooking_base_url');
class NFBooking {
	public function __construct() {
		add_shortcode('booking_miamia', [$this, 'garung_booking_miamia_load']);
		add_action( 'widgets_init', [$this, 'create_booking_widget'] );
		add_action( 'plugins_loaded', [$this, 'garung_booking_miamia_load'] );
		add_action('init',[$this, 'recieve_data'] );
		add_action('admin_menu', [$this, 'booking_admin_settings']);
		
	}
	public function booking_admin_settings() {
		add_submenu_page( 'options-general.php', 'Booking Module', 'Booking Module', 'manage_options', 'booking-module.php', [$this, 'settingPage'] );
	}
	public function settingPage() {
		if(isset($_POST['booking-form-url-submit'])) {
			update_option(NFBOOKING_URL, $_POST['booking_form_url'], true);
		}
		$booking_url = get_option(NFBOOKING_URL);
		
echo <<<EOF
	<h3>Booking Module Settings</h3>
	<form method="post">
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="first_name">Base Booking Form URL</label></th>
				<td><input type="url" class="regular-text" value="{$booking_url}" id="booking-form-url" name="booking_form_url"></td>
			</tr>
			<tr>
				<td>
					<input type="submit" class="button button-primary" name="booking-form-url-submit" value="Save">
				</td>
			</tr>
		</tbody>
	</table>
	</form>
EOF;
	} 
	public function garung_booking_miamia_load( $settings = array() )
	{	
		$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

		global $gdlr_spaces, $hotel_option;
		$margin = (!empty($settings['margin-bottom']) && 
			$settings['margin-bottom'] != $gdlr_spaces['bottom-blog-item'])? 'margin-bottom: ' . $settings['margin-bottom'] . ';': '';
		$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
		//================== time =================
		$current_date = current_time('Y-m-d');
		$next_date = date('Y-m-d', strtotime($current_date . "+1 days"));
		// ========================================
		$value = array(
			'gdlr-check-in' => $current_date1,
			'gdlr-night' => 1,
			'gdlr-check-out' => $next_date,
			'gdlr-room-number' => 1,
			'gdlr-adult-number' => 1,
			'gdlr-children-number' => 0
		);
		
		$ret  = $this->gdlr_get_item_title($settings);	

		$ret .= '<div class="gdlr-hotel-availability-wrapper';
		if( !empty($hotel_option['enable-hotel-branch']) && $hotel_option['enable-hotel-branch'] == 'enable' ){
			$ret .= ' gdlr-hotel-branches-enable';
		}
		$ret .= '" ' . $margin_style . $item_id . ' >';
		$ret .= '<form class="gdlr-hotel-availability gdlr-item" id="gdlr-hotel-availability" method="post">';
		$ret .= '<input type="hidden" name="Rd" value="1" >';
		$ret .= $this->gdlr_get_reservation_datepicker(array(
			'title'=>__('Check In', 'gdlr-hotel'),
			'slug'=>'gdlr-check-in',
			'id'=>'gdlr-check-in',
			'value'=>$value['gdlr-check-in']
		));
		$ret .= $this->gdlr_get_reservation_combobox(array(
			'title'=>__('Night', 'gdlr-hotel'),
			'slug'=>'gdlr-night',
			'id'=>'gdlr-night',
			'value'=>$value['gdlr-night']
		));
		$ret .= $this->gdlr_get_reservation_datepicker(array(
			'title'=>__('Check Out', 'gdlr-hotel'),
			'slug'=>'gdlr-check-out',
			'id'=>'gdlr-check-out',
			'value'=>$value['gdlr-check-out']
		));
		$ret .= $this->gdlr_get_reservation_combobox(array(
			'title'=>__('Adults', 'gdlr-hotel'),
			'slug'=>'gdlr-adult-number',
			'id'=>'gdlr-adult-number',
			'value'=>$value['gdlr-adult-number']
		));
		$ret .= $this->gdlr_get_reservation_combobox(array(
			'title'=>__('Children', 'gdlr-hotel'),
			'slug'=>'gdlr-children-number',
			'id'=>'gdlr-children-number',
			'value'=>$value['gdlr-children-number']
		));
		$ret .= '<div class="gdlr-hotel-availability-submit" >';
		$ret .= '<input type="submit" style="background-color: #323535;" name="book_new_room" class="gdlr-reservation-bar-button gdlr-button with-border" value="' . __('Check Availability', 'gdlr-hotel') . '" >';
		$ret .= '</div>';
		
		$ret .= '<div class="clear"></div>';
		$ret .= '</form>';
		$ret .= '</div>';
		
		return $ret;
	}
	public function gdlr_get_item_title( $atts )
	{
		$ret = '';
		
		$atts['type'] = (empty($atts['type']))? '': $atts['type'];
		$atts['title-type'] = (empty($atts['title-type']))? $atts['type']: $atts['title-type'];
	
		if( !empty($atts['title-type']) && $atts['title-type'] != 'none' && 
			(!empty($atts['title']) || !empty($atts['caption']) || !empty($atts['carousel'])) ){
			$item_class  = 'pos-' . str_replace('-divider', '', $atts['title-type']);
			$item_class .= (!empty($atts['carousel']))? ' gdlr-nav-container': '';
			if( strpos($atts['title-type'], '-divider') > 0 ){
				$item_class .= ' pos-' . $atts['title-type'];
			}
			
			$ret .= '<div class="gdlr-item-title-wrapper gdlr-item ' . $item_class . ' ">';
			
			$ret .= '<div class="gdlr-item-title-head">';
			if(!empty($atts['title'])){
				$ret .= '<h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">' . $atts['title'] . '</h3>';
			}
			if( !empty($atts['carousel']) ){
				$ret .= '<div class="gdlr-item-title-carousel" >';
				$ret .= '<i class="icon-angle-left gdlr-flex-prev"></i>';
				$ret .= '<i class="icon-angle-right gdlr-flex-next"></i>';
				$ret .= '</div>';
			}
			$ret .= '<div class="clear"></div>';
			$ret .= '</div>';
			
			$ret .= (strpos($atts['title-type'], 'divider') > 0)? '<div class="gdlr-item-title-divider"></div>': '';
			
			if(!empty($atts['caption'])){
				$ret .= '<div class="gdlr-item-title-caption gdlr-title-font gdlr-skin-info">' . $atts['caption'] . '</div>';
			}
			
			if(!empty($atts['right-text']) && !empty($atts['right-text-link'])){
				$ret .= '<a class="gdlr-item-title-link" href="' . esc_url($atts['right-text-link']) . '" >';
				$ret .= $atts['right-text'] . '<i class="fa fa-long-arrow-right icon-long-arrow-right" ></i></a>';
			}
			
			$ret .= '</div>'; // gdlr-item-title-wrapper
		}
		return $ret;
	}
	public function gdlr_get_reservation_combobox($option, $min_num = 0, $max_num = 10)
	{
		$ret  = '<div class="gdlr-reservation-field gdlr-resv-combobox">';
		$ret .= '<span class="gdlr-reservation-field-title">' . $option['title'] . '</span>';
		$ret .= '<div class="gdlr-combobox-wrapper">';
		$ret .= '<select name="' . $option['slug'] . (empty($option['multiple'])? '': '[]') . '" ';
		$ret .= !empty($option['id'])? 'id="' . $option['id'] . '" >': '>';
		for( $i=$min_num; $i<$max_num; $i++ ){
			$ret .= '<option value="' . $i . '" ' . ((!empty($option['value']) && $i==$option['value'])? 'selected':'') . ' >' . $i . '</option>';
		}
		if( !empty($option['value']) && $option['value'] >= $max_num ){
			$ret .= '<option value="' . $option['value'] . '" selected >' . $option['value'] . '</option>';
		}
		$ret .= '</select>';
		$ret .= '</div>'; 
		$ret .= '</div>';			
		return $ret;
	}
	public function gdlr_get_reservation_datepicker($option){
		global $theme_option;
		
		$ret  = '<div class="gdlr-reservation-field gdlr-resv-datepicker">';
		$ret .= '<span class="gdlr-reservation-field-title">' . $option['title']  . '</span>';
		$ret .= '<div class="gdlr-datepicker-wrapper">';
		$ret .= '<input type="text"  id="' . $option['id'] . '" class="gdlr-datepicker" ';
		$ret .= (empty($theme_option['datepicker-format']))? '': 'data-dfm="d/m/yy" ';
		$ret .= (empty($option['value'])? '': 'value="' . $option['value'] . '" ') . '/>';
		//var_dump($option['value']);
		$ret .= '<input type="hidden" class="gdlr-datepicker-alt abc" name="' . $option['slug'] . '" ';
		$ret .= (empty($option['value'])? '' :'value="'. $option['value'] . '" ') . '/>';
		$ret .= '</div>'; // gdlr-datepicker-wrapper
		$ret .= '</div>';
		return $ret;
	}

	public function create_booking_widget() 
	{
	    register_widget('booking_miamia_widget');
	}
	public function garung_get_reservation_bar($single_form = false)
	{
		global $hotel_option;
		
		$ret  = '<form class="gdlr-reservation-bar" id="gdlr-reservation-bar" method="post">';
		$ret .= '<div class="gdlr-reservation-bar-title">' . __('Your Reservation', 'gdlr-hotel') . '</div>';
		
		if( !empty($_GET['state']) && $_GET['state'] == 4 && !empty($_GET['invoice']) ){
			global $wpdb;
			$temp_sql  = "SELECT contact_info, booking_data FROM " . $wpdb->prefix . "gdlr_hotel_payment ";
			$temp_sql .= "WHERE id = " . $_GET['invoice'];	
			$result = $wpdb->get_row($temp_sql);
			$data = unserialize($result->booking_data);
			$contact = unserialize($result->contact_info);
			
			$ret .= '<div class="gdlr-reservation-bar-summary-form" id="gdlr-reservation-bar-summary-form" style="display: block;">';
			$ret .= $this->gdlr_get_summary_form($data, false, $contact['coupon']);
			$ret .= '</div>';
		}else{
			$ret .= '<div class="gdlr-reservation-bar-summary-form" id="gdlr-reservation-bar-summary-form"></div>';
			
			if( !empty($_POST['hotel_data']) ){
				$ret .= '<div class="gdlr-reservation-bar-room-form gdlr-active" id="gdlr-reservation-bar-room-form" style="display: block;">';
				$ret .= $this->gdlr_get_reservation_room_form($_POST, 0);
				$ret .= '</div>';
			}else{
				$ret .= '<div class="gdlr-reservation-bar-room-form" id="gdlr-reservation-bar-room-form"></div>';
			}
			
			$ret .= '<div class="gdlr-reservation-bar-date-form" id="gdlr-reservation-bar-date-form">';
			$ret .= $this->gdlr_get_reservation_date_form($single_form);
			$ret .= '</div>';
			
			$ret .= '<div class="gdlr-reservation-bar-service-form" id="gdlr-reservation-bar-service-form"></div>';
		}
		
		if( $single_form ){
			$ret .= '<input type="hidden" name="single-room" value="' . get_the_ID() . '" />';
		}else if( !empty($_POST['single-room']) ){
			$ret .= '<input type="hidden" name="single-room" value="' . $_POST['single-room'] . '" />';
		}
		$ret .= '<input type="hidden" name="Rd" value="1" >';
		$ret .= '</form>';
		return $ret;
	}
	public function gdlr_get_reservation_room_form($data, $selected_room){
		$ret  = ''; $active = false;
		
		if( !empty($data['gdlr-room-id']) ){
			for( $i=0; $i<sizeOf($data['gdlr-room-id']) && $i<$data['gdlr-room-number']; $i++ ){
				$options = array(
					'room-number'=>$i + 1, 
					'room-id'=>$data['gdlr-room-id'][$i], 
					'adult'=>$data['gdlr-adult-number'][$i], 
					'children'=>$data['gdlr-children-number'][$i],
					'already_active'=>$active
				);
				if( $selected_room == $i || empty($data['gdlr-room-id'][$i]) ){
					$active = true;
					$options['room-id'] = '';
				}
				$ret .= gdlr_get_reservation_room($options);					
			}
		}
		
		if( empty($data['gdlr-room-id']) || 
			(!$active && $selected_room >= sizeOf($data['gdlr-room-id']) && $selected_room < intval($data['gdlr-room-number'])) ){
			$ret .= gdlr_get_reservation_room(array(
				'room-number'=>intval($selected_room) + 1, 
				'room-id'=>'', 
				'adult'=>$data['gdlr-adult-number'][$selected_room], 
				'children'=>$data['gdlr-children-number'][$selected_room]
			));
		}
		return $ret;
	}
	public function gdlr_get_reservation_date_form($single_form = false, $data = array()){
		$ret  = '';
		if( !empty($_POST['hotel_data']) ){
			$value = $_POST;
		}else{
			$current_date = current_time('Y-m-d');
			$next_date = date('Y-m-d', strtotime($current_date . "+1 days"));
			
			$value = array(
				'gdlr-check-in' => $current_date,
				'gdlr-night' => 1,
				'gdlr-check-out' => $next_date,
				'gdlr-room-number' => 1,
				'gdlr-adult-number' => 1,
				'gdlr-children-number' => 0
			);
			
			// for single room page
			global $gdlr_post_option;
			if( !empty($gdlr_post_option['max-people']) && intval($gdlr_post_option['max-people']) < 2 ){
				$value['gdlr-adult-number'] = 1;
			} 
		}
		// date
		$ret .= $this->gdlr_get_reservation_datepicker(array(
			'title'=>__('Check In', 'gdlr-hotel'),
			'slug'=>'gdlr-check-in',
			'id'=>'gdlr-check-in',
			'value'=>$value['gdlr-check-in']
		));
		// night
		$ret .= $this->gdlr_get_reservation_combobox(array(
			'title'=>__('Night', 'gdlr-hotel'),
			'slug'=>'gdlr-night',
			'id'=>'gdlr-night',
			'value'=>$value['gdlr-night']
		), 1);
		$ret .= '<div class="clear"></div>';
		// checkout
		$ret .= $this->gdlr_get_reservation_datepicker(array(
			'title'=>__('Check Out', 'gdlr-hotel'),
			'slug'=>'gdlr-check-out',
			'id'=>'gdlr-check-out',
			'value'=>$value['gdlr-check-out']
		));
		$ret .= '<div class="clear"></div>';
		// adult
		$ret .= $this->gdlr_get_reservation_combobox(array(
			'title'=>__('Adults', 'gdlr-hotel'),
			'slug'=>'gdlr-adult-number',
			'id'=>'gdlr-adult-number',
			'value'=>$value['gdlr-adult-number']
		));
		$ret .= '<div class="clear"></div>';
		// children
		$ret .= $this->gdlr_get_reservation_combobox(array(
			'title'=>__('Children', 'gdlr-hotel'),
			'slug'=>'gdlr-children-number',
			'id'=>'gdlr-children-number',
			'value'=>$value['gdlr-children-number']
		));
		$ret .= '</div>'; // gdlr-reservation-people-amount-wrapper
		$ret .= '<div class="clear"></div>';
		
		if( $single_form ){
			$ret .= '<input type="hidden" name="hotel_data" value="1" >';
			$ret .= '<input type="submit" name="book_new_room" style="margin: 0 auto 10px 24%;" class="gdlr-reservation-bar-button gdlr-button with-border" value="' . __('Check Availability', 'gdlr-hotel') . '" >';
		}else if( empty($_POST['hotel_data']) ){
			$ret .= '<input type="submit" id="gdlr-reservation-bar-button" style="margin: 0 auto 10px 24%; " name="book_new_room" class="gdlr-reservation-bar-button gdlr-button with-border" value="' . __('Check Availability', 'gdlr-hotel') . '" >';
		}
		$ret .= '<div class="clear"></div>';
		return $ret;
	}
	public function recieve_data()
	{
		if(isset($_POST) && isset($_POST['book_new_room'])) {
			$check_in = new DateTime($_POST['gdlr-check-in']);
			$time_checkin = $check_in->format('m/d/Y');

			$check_out = new DateTime($_POST['gdlr-check-out']);
			$time_checkout = $check_out->format('m/d/Y');

			if(empty($_POST['gdlr-adult-number']) || empty($_POST['gdlr-children-number']))
			{
				if(empty($_POST['gdlr-adult-number']))
				{
					if(empty($_POST['gdlr-children-number']))
					{
						$url_booking = get_option(NFBOOKING_URL).'?Rd='.$_POST['Rd'].'&A='.$time_checkin.'&D='.$time_checkout.'#/rate';
					}
				}
				else
				{
					if(empty($_POST['gdlr-children-number']))
					{
						$url_booking = get_option(NFBOOKING_URL).'?Rd='.$_POST['Rd'].'&A='.$time_checkin.'&D='.$time_checkout.'&Ad='.$_POST['gdlr-adult-number'].'#/rate';
					}
					else
					{
						$url_booking = get_option(NFBOOKING_URL).'?Rd='.$_POST['Rd'].'&A='.$time_checkin.'&D='.$time_checkout.'&Ad='.$_POST['gdlr-adult-number'].'&C='.$_POST['gdlr-children-number'].'#/rate';
					}
				}
			}
			else
			{
				$url_booking = get_option(NFBOOKING_URL).'?Rd='.$_POST['Rd'].'&A='.$time_checkin.'&D='.$time_checkout.'&Ad='.$_POST['gdlr-adult-number'].'&C='.$_POST['gdlr-children-number'].'#/rate';
			}
			header("Location:".$url_booking, true);
			exit();
		}
	}
}
new NFBooking;

//====================== redirect form ======================



/**
* widget : Booking miamia custom - child "Room Hotel"
*/
class booking_miamia_widget extends WP_Widget
{
	public $url_booking_post;
	function __construct() {
 		parent::__construct (
	        'booking_widget',
	        'Booking Miamia Widget',
	 
	        array(
	          'description' => 'Booking right sidebar for Single Room page'
	        )
	    );
    }
    function form($instance)
    {
    	echo "Active booking right sidebar for Single Room page"; 
    }

    function widget( $args, $instance) {
 		echo $before_widget;
 		$bk = new NFBooking();
 		echo $bk->garung_get_reservation_bar(false);
 		echo $after_widget;	
    }
}

function booking_widgets_init() {
 
 	register_sidebar( array(
	   'name' => __( 'Booking Right Sidebar', 'gdlr-hotel' ),
	   'id' => 'booking_right_sidebar',
	   'description' => __( 'Booking Right Sidebar', 'gdlr-hotel' ),
	   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	   'after_widget' => '</aside>',
	   'before_title' => '<h3 class="widget-title">',
	   'after_title' => '</h3>',
	   ) 
 	);
}
 
add_action( 'widgets_init', 'booking_widgets_init' );
