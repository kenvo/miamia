=== Booking Miamia ===
Requires at least: 4
Stable tag: Hotel, booking
License: GPLv2

This is child plugin of Goodlayder Hotel

== Description ==
Shortcode: [booking_miamia][booking_miamia]

Widget: Pull Booking Miamia Widget into Booking Right Sidebar to active right side bar of Single Room page

Setting: Setting/Booking Module, enter Booking Url use link RMS API, example: https://bookings.rms.com.au/obookings3/Search/Index/7630/1/


== Installation ==
First, install Goodlayers Hotel Plugin
Next, install this plugin.
